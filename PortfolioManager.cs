public class PortfolioManager
{
    private WindForecast windForecast; 
    private RealTimeData realTimeData;
    private DataObserver dataObserver;
    private DataPersister dataPersister;
    public int id; 
    public string name; 
    public string portfolioName; 

    public static readonly decimal SIGNIFICANT_COEFFICIENT; 

    public PortfolioManager(DataObserver dataObserver, DataPersister dataPersister)
    {
        windForecast = dataObserver.Data.WindForecast;
        realTimeData = dataObserver.Data.RealTimeData;
        SIGNIFICANT_COEFFICIENT = 0.123;
    }

    public bool monitorData()
    {
        decimal windForecastIndex = windForecast.getIndex();
        decimal realDataIndex = realTimeData.getIndex();

        decimal diff = windForecastIndex - realDataIndex; 

        if(diff < 0){
            throw new Exception("The sum of wind forecast and forecast corrections should be limited to min 0MW.");
        }

        if(Math.Abs(diff) > SIGNIFICANT_COEFFICIENT){

            correctData(dataObserver.Data, diff);
        }
    }

    public bool correctData(Data data, decimal diff){

        Result result = data.manualForecastCorrections(diff);
        
        if(result.manualForecastCorrectionsResult > 300){
            return;
        }
        this.dataPersister.save(result.perControlArea);
    }
}
